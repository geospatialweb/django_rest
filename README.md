# Vue.js | Django | Django REST Framework

Clone the repo.

Create Python virtual environment: ```$ cd project && python3 -m venu venu```\
Activate Python virtual environment: ```$ source venv/bin/activate```

Install Django dependencies: ```$ pip install -r requirements.txt```\
Install Vue.js dependencies: ```$ cd frontend && npm install```

Start Django Vue development server in one terminal: ```$ cd project && python3 manage.py runserver```\
Start Vue development server in another terminal: ```$ cd project/frontend && npm run serve```

The front-end UI interface is created in Vue.js and Bootstrap 4. The back-end is developed in Django and Django Rest framework with both session and token user authentication. CSRF token security is also employed for web browser access.

QuestionTime code is written by Michele Saba: <https://www.udemy.com/course/the-complete-guide-to-django-rest-framework-and-vue-js/>
